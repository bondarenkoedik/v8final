#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>
#include <thread>
#include <mutex>
#include <iostream>
#include <chrono>
#include <sstream>
#include <tuple>
#include <omp.h>
#include <fstream>

#include "threadpool.h"

#include "v8wrap.h"

std::string _readFile(const char* filename);

int main(int argc, char* argv[]) {
  std::string pathToLibs("/home/eduardb/Documents/projects/v8/v8/mytest/prod/"); // absolute path here
  initialize(argc, argv, pathToLibs.c_str());

  const std::string conv_1 = "conv_1";
  const std::string node_1 = "node_1";
  const std::string conv_2 = "conv_2";
  const std::string node_2 = "node_2";

  // const std::string data1 = "(function(data){ for(let i = 0; i < 999000; i++); data.a += 1; return data; })";
  const std::string data1 = "(function(data){(typeof data.i === \"undefined\") ? data.i=0 : data.i++; return data;})";
  // const std::string data2 = "(function(data){(typeof data.i === \"undefined\") ? data.i=0 : data.i++; return data;})";
  // const std::string data2 = "(function(data){ for(let i = 0; i < 100000; i++); data.a += 2; return data; })";

  auto compile_res1 = compile(conv_1.c_str(), node_1.c_str(), data1.c_str());
  std::cout << std::get<0>(compile_res1) << " " << std::get<1>(compile_res1) << std::endl;
  auto compile_res2 = compile(conv_2.c_str(), node_2.c_str(), data1.c_str());
  std::cout << std::get<0>(compile_res2) << " " << std::get<1>(compile_res2) << std::endl;

  struct timespec start, finish;
  double elapsed;

  // std::string bigJSON = _readFile("./out.json");
  // std::cout << bigJSON << std::endl;

  clock_gettime(CLOCK_MONOTONIC, &start);

  // "{\"a\": 1, \"b\": 2, \"arr\": [1, 2, 3]}"

  #pragma omp parallel for num_threads(2)
  for (int i = 0; i < 20000; i++) {
    int tid = omp_get_thread_num();
    if (tid == 0) {
      auto run_res1 = run(conv_1.c_str(), node_1.c_str(), "{\"a\": 1, \"b\": 2, \"arr\": [1, 2, 3]}");
      // std::cout << std::get<0>(run_res1) << " " << std::get<1>(run_res1) << std::endl;
    } else {
      auto run_res2 = run(conv_2.c_str(), node_2.c_str(), "{\"a\": 1, \"b\": 2, \"arr\": [1, 2, 3]}");
      // std::cout << std::get<0>(run_res2) << " " << std::get<1>(run_res2) << std::endl;
    }
  }

  // for (int i = 0; i < 10000; i++) {
  //   // auto run_res1 = run(conv_1.c_str(), node_1.c_str(), "{\"a\": 1, \"b\": 2, \"arr\": [1, 2, 3]}");
    // auto run_res1 = run(conv_1.c_str(), node_1.c_str(), bigJSON.c_str());
  //   // run_res1 = run(conv_1.c_str(), node_1.c_str(), "{\"a\": 1, \"b\": 2, \"arr\": [1, 2, 3]}");
  //   // std::cout << std::get<0>(run_res1) << " " << std::get<1>(run_res1) << std::endl;
  //   // auto run_res2 = run(conv_2.c_str(), node_2.c_str(), "{\"a\": 1, \"b\": 2, \"arr\": [1, 2, 3]}");
  //   // std::cout << std::get<0>(run_res2) << " " << std::get<1>(run_res2) << std::endl;
  // }

  // auto run_res1 = run(conv_1.c_str(), node_1.c_str(), bigJSON.c_str());

  clock_gettime(CLOCK_MONOTONIC, &finish);
  elapsed = (finish.tv_sec - start.tv_sec);
  elapsed += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;

  // elapsed = timespecDiff(&finish, &start);

  std::cout << std::fixed << elapsed << std::endl;

}

std::string _readFile(const char* filename) {
  std::string fullPath = std::string(filename);
  std::ifstream t(fullPath);
  std::stringstream buffer;
  buffer << t.rdbuf();
  return buffer.str();
}
