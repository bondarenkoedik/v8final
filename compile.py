import os

commands = [
    "g++ -O2 -c -o ./obj/v8wrap.o -Wall -Werror -fpic v8wrap.cpp -I ./ -I ../../include/",
    "g++ -O2 -shared -o ./lib/libv8wrap.so ./obj/v8wrap.o -L ../../../icu/build/lib -lpthread -licuuc -licui18n -licuio -licule -licudata",
    "g++ -O2 -o ./bin/cserver -I ./ -I /usr/local/opt/erlang/lib/erlang/lib/erl_interface-3.9.1/include/ -L ../../../icu/build/lib/ -L ./lib -L ./v8 -L /usr/local/opt/erlang/lib/erlang/lib/erl_interface-3.9.1/lib complex.cpp cnode.cpp ./v8/*.o -lerl_interface -lei -lnsl -lpthread -licuuc -licui18n -licuio -licule -licudata -lv8wrap -lv8 -std=c++11"
];

for command in commands:
    print(command)
    os.system(command)

# g++ -o ../bin/cserver -I ./ -I /usr/local/opt/erlang/lib/erlang/lib/erl_interface-3.9.1/include/ -L ../../../../icu/build/lib/ -L ../lib -L ../v8 -L /usr/local/opt/erlang/lib/erlang/lib/erl_interface-3.9.1/lib complex.cpp cnode_s.cpp ../v8/*.o -lerl_interface -lei -lnsl -lpthread -licuuc -licui18n -licuio -licule -licudata -ltest -lv8 -std=c++11
