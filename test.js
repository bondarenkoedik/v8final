var fs = require('fs');
// var content = fs. readFileSync('./out.json', 'utf8');
var content = '{"a": 1, "b": 2, "arr": [1, 2, 3]}';

var a = function(data){ (typeof data.i === "undefined") ? data.i=0 : data.i++; return data;}; // func1
// var a = function(data){ for(let i = 0; i < 999000; i++); data.a += 1; return data; }; // func2
// var a = eval("(function(data){ let moment = require('libs/moment.js'); /*dsadas*/ moment(); for(let i = 0; i < 999000; i++); data.a += 1; return data; })"); // func2
// console.log(a({}));
// console.log("OK");
console.time('someFunction');
for (let i = 0; i < 10000; i++) { a(JSON.parse(content)); a(JSON.parse(content));}
console.timeEnd('someFunction');

// node - 3.194ms vs my - 168.419ms func1
// node - 39603.165ms vs my 40542.075 func2

// with big JSON
// node - 173786.490ms vs my - 380112.343 func1, 204290.203
// node - 206544.342ms vs my - 421905.401 func2, 248.393919
