#ifndef V8WRAP_H
#define V8WRAP_H

#include <tuple>
#include <string>

void initialize(int, char**, const char*);
std::tuple<int, std::string> compile(const char*, const char*, const char*);
std::tuple<int, std::string> run(const char*, const char*, const char*);
void deinitialize();

#endif
