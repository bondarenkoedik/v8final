/* cnode_s2.c */

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>
#include <thread>
#include <mutex>
#include <iostream>
#include <chrono>
#include <sstream>

#include "erl_interface.h"
#include "ei.h"

#include "threadpool.h"

#include "v8wrap.h"

#define BUFSIZE 1000

#define ERROR_CODE 0
#define RESPONSE 1

int my_listen(int);

void process(int fd, ErlMessage* emsg) {

  ETERM *fromp = NULL,
        *tuplep = NULL,
        *func = NULL,
        *conv_id_term = NULL,
        *node_id_term = NULL,
        *data_term = NULL,
        *resp = NULL;

  fromp = erl_element(2, emsg->msg);
  tuplep = erl_element(3, emsg->msg);

  func = erl_element(1, tuplep);

  conv_id_term = erl_element(2, tuplep);
  node_id_term = erl_element(3, tuplep);
  data_term = erl_element(4, tuplep);

  char* conv_id_c = erl_iolist_to_string(conv_id_term);
  char* node_id_c = erl_iolist_to_string(node_id_term);
  // if compile - data is a source code
  // if run - data is a json
  char* data = erl_iolist_to_string(data_term);

  std::tuple<int, std::string> res;
  if (strncmp(ERL_ATOM_PTR(func), "compile", 7) == 0) {
    res = compile(conv_id_c, node_id_c, data);
  } else if (strncmp(ERL_ATOM_PTR(func), "run", 3) == 0) {
    res = run(conv_id_c, node_id_c, data);
  }

  resp = erl_format("{cnode, ~i, ~b}", std::get<ERROR_CODE>(res), std::get<RESPONSE>(res).c_str());
  erl_send(fd, fromp, resp);

  erl_free_term(emsg->from); erl_free_term(emsg->msg);
  erl_free_term(fromp); erl_free_term(tuplep);
  erl_free_term(func);
  erl_free_term(conv_id_term); erl_free_term(node_id_term); erl_free_term(data_term);
  erl_free(conv_id_c); erl_free(node_id_c); erl_free(data);
  erl_free_term(resp);

  delete emsg;
}

int main(int argc, char **argv) {

  std::string pathToLibs("/home/eduardb/Documents/projects/v8/v8/mytest/prod/"); // absolute path here
  initialize(argc, argv, pathToLibs.c_str());

  struct in_addr addr;                     /* 32-bit IP number of host */
  int port;                                /* Listen port number */
  int listen;                              /* Listen socket */
  int fd;                                  /* fd to Erlang node */
  ErlConnect conn;                         /* Connection data */

  int loop = 1;                            /* Loop flag */
  int got;                                 /* Result of receive */
  unsigned char buf[BUFSIZE];              /* Buffer for incoming message */

  port = atoi(argv[1]);

  erl_init(NULL, 0);

  addr.s_addr = inet_addr("192.168.0.104");
  if (erl_connect_xinit("idril", "cnode", "cnode@192.168.0.104",
			&addr, "secretcookie", 0) == -1)
    erl_err_quit("erl_connect_xinit");

  /* Make a listen socket */
  if ((listen = my_listen(port)) <= 0)
    erl_err_quit("my_listen");

  if (erl_publish(port) == -1)
    erl_err_quit("erl_publish");

  if ((fd = erl_accept(listen, &conn)) == ERL_ERROR)
    erl_err_quit("erl_accept");
  fprintf(stderr, "Connected to %s\n\r", conn.nodename);

  nbsdx::concurrent::ThreadPool<std::function<void(int, ErlMessage*)>, 1> pool; // Defaults to 10 threads.

  while (loop) {

    ErlMessage* emsg = new ErlMessage();
    got = erl_receive_msg(fd, buf, BUFSIZE, emsg);

    if (got == ERL_TICK) {
      /* ignore */
      delete emsg;
    } else if (got == ERL_ERROR) {
      delete msg;
      loop = 0;
    } else {

      if (emsg->type == ERL_REG_SEND) {
        pool.AddJob(process, fd, emsg);
      }
    }
  }

  deinitialize();

}


int my_listen(int port) {
  int listen_fd;
  struct sockaddr_in addr;
  int on = 1;

  if ((listen_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    return (-1);

  setsockopt(listen_fd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));

  memset((void*) &addr, 0, (size_t) sizeof(addr));
  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);
  addr.sin_addr.s_addr = htonl(INADDR_ANY);

  if (bind(listen_fd, (struct sockaddr*) &addr, sizeof(addr)) < 0)
    return (-1);

  listen(listen_fd, 5);
  return listen_fd;
}
